title: Terminal
background_opacity: 0.8

env:
  TERM: xterm-256color

window:
  dynamic_padding: true
  padding:
    x: 30
    y: 30

cursor:
  hide_when_typing: true

font:
  normal:
    family: Hack Nerd Font
    style: Regular

  bold:
    family: Hack Nerd Font
    style: Bold

  italic:
    family: Hack Nerd Font
    style: Italic

  bold_italic:
    family: Hack Nerd Font
    style: Bold Italic

  size: 11.0

  offset:
    x: 0
    y: -1

key_bindings:
  - { key: Return,   mods: Control|Shift, action: SpawnNewInstance } # Spawn new instance in same directory

schemes:

  afterglow: &afterglow
    primary:
      background: '0x2c2c2c'
      foreground: '0xd6d6d6'
      dim_foreground:    '0xdbdbdb'
      bright_foreground: '0xd9d9d9'
      dim_background:    '0x202020' # not sure
      bright_background: '0x3a3a3a' # not sure
    cursor:
      text:   '0x2c2c2c'
      cursor: '0xd9d9d9'
    normal:
      black:   '0x1c1c1c'
      red:     '0xbc5653'
      green:   '0x909d63'
      yellow:  '0xebc17a'
      blue:    '0x7eaac7'
      magenta: '0xaa6292'
      cyan:    '0x86d3ce'
      white:   '0xcacaca'
    bright:
      black:   '0x636363'
      red:     '0xbc5653'
      green:   '0x909d63'
      yellow:  '0xebc17a'
      blue:    '0x7eaac7'
      magenta: '0xaa6292'
      cyan:    '0x86d3ce'
      white:   '0xf7f7f7'
    dim:
      black:   '0x232323'
      red:     '0x74423f'
      green:   '0x5e6547'
      yellow:  '0x8b7653'
      blue:    '0x556b79'
      magenta: '0x6e4962'
      cyan:    '0x5c8482'
      white:   '0x828282'

  argonaut: &argonaut
    primary:
      background: '0x292C3E'
      foreground: '0xEBEBEB'
    cursor:
      text: '0xFF261E'
      cursor: '0xFF261E'
    normal:
      black:   '0x0d0d0d'
      red:     '0xFF301B'
      green:   '0xA0E521'
      yellow:  '0xFFC620'
      blue:    '0x1BA6FA'
      magenta: '0x8763B8'
      cyan:    '0x21DEEF'
      white:   '0xEBEBEB'
    bright:
      black:   '0x6D7070'
      red:     '0xFF4352'
      green:   '0xB8E466'
      yellow:  '0xFFD750'
      blue:    '0x1BA6FA'
      magenta: '0xA578EA'
      cyan:    '0x73FBF1'
      white:   '0xFEFEF8'

  ayu_dark: &ayu_dark
    primary:
      background: '0x0A0E14'
      foreground: '0xB3B1AD'
    normal:
      black:   '0x01060E'
      red:     '0xEA6C73'
      green:   '0x91B362'
      yellow:  '0xF9AF4F'
      blue:    '0x53BDFA'
      magenta: '0xFAE994'
      cyan:    '0x90E1C6'
      white:   '0xC7C7C7'
    bright:
      black:   '0x686868'
      red:     '0xF07178'
      green:   '0xC2D94C'
      yellow:  '0xFFB454'
      blue:    '0x59C2FF'
      magenta: '0xFFEE99'
      cyan:    '0x95E6CB'
      white:   '0xFFFFFF'

  base16_dark: &base16_dark
    primary:
      background: '0x181818'
      foreground: '0xd8d8d8'
    cursor:
      text: '0xd8d8d8'
      cursor: '0xd8d8d8'
    normal:
      black:   '0x181818'
      red:     '0xab4642'
      green:   '0xa1b56c'
      yellow:  '0xf7ca88'
      blue:    '0x7cafc2'
      magenta: '0xba8baf'
      cyan:    '0x86c1b9'
      white:   '0xd8d8d8'
    bright:
      black:   '0x585858'
      red:     '0xab4642'
      green:   '0xa1b56c'
      yellow:  '0xf7ca88'
      blue:    '0x7cafc2'
      magenta: '0xba8baf'
      cyan:    '0x86c1b9'
      white:   '0xf8f8f8'

  blood_moon: &blood_moon
    primary:
      background: '0x10100E'
      foreground: '0xC6C6C4'
    normal:
      black:   '0x10100E'
      red:     '0xC40233'
      green:   '0x009F6B'
      yellow:  '0xFFD700'
      blue:    '0x0087BD'
      magenta: '0x9A4EAE'
      cyan:    '0x20B2AA'
      white:   '0xC6C6C4'
    bright:
      black:   '0x696969'
      red:     '0xFF2400'
      green:   '0x03C03C'
      yellow:  '0xFDFF00'
      blue:    '0x007FFF'
      magenta: '0xFF1493'
      cyan:    '0x00CCCC'
      white:   '0xFFFAFA'

  campbell: &campbell
    primary:
      background: '0x0c0c0c'
      foreground: '0xcccccc'
    normal:
      black:      '0x0c0c0c'
      red:        '0xc50f1f'
      green:      '0x13a10e'
      yellow:     '0xc19c00'
      blue:       '0x0037da'
      magenta:    '0x881798'
      cyan:       '0x3a96dd'
      white:      '0xcccccc'
    bright:
      black:      '0x767676'
      red:        '0xe74856'
      green:      '0x16c60c'
      yellow:     '0xf9f1a5'
      blue:       '0x3b78ff'
      magenta:    '0xb4009e'
      cyan:       '0x61d6d6'
      white:      '0xf2f2f2'

  hyper: &hyper
    primary:
      background: '0x000000'
      foreground: '0xffffff'
    cursor:
      text: '0xF81CE5'
      cursor: '0xffffff'
    normal:
      black:   '0x000000'
      red:     '0xfe0100'
      green:   '0x33ff00'
      yellow:  '0xfeff00'
      blue:    '0x0066ff'
      magenta: '0xcc00ff'
      cyan:    '0x00ffff'
      white:   '0xd0d0d0'
    bright:
      black:   '0x808080'
      red:     '0xfe0100'
      green:   '0x33ff00'
      yellow:  '0xfeff00'
      blue:    '0x0066ff'
      magenta: '0xcc00ff'
      cyan:    '0x00ffff'
      white:   '0xFFFFFF'

  material_theme: &material_theme
    primary:
      background: '0x1e282d'
      foreground: '0xc4c7d1'
    normal:
      black:   '0x666666'
      red:     '0xeb606b'
      green:   '0xc3e88d'
      yellow:  '0xf7eb95'
      blue:    '0x80cbc4'
      magenta: '0xff2f90'
      cyan:    '0xaeddff'
      white:   '0xffffff'
    bright:
      black:   '0xff262b'
      red:     '0xeb606b'
      green:   '0xc3e88d'
      yellow:  '0xf7eb95'
      blue:    '0x7dc6bf'
      magenta: '0x6c71c4'
      cyan:    '0x35434d'
      white:   '0xffffff'

  nord: &nord
    primary:
      background: '0x2E3440'
      foreground: '0xD8DEE9'
    normal:
      black:   '0x3B4252'
      red:     '0xBF616A'
      green:   '0xA3BE8C'
      yellow:  '0xEBCB8B'
      blue:    '0x81A1C1'
      magenta: '0xB48EAD'
      cyan:    '0x88C0D0'
      white:   '0xE5E9F0'
    bright:
      black:   '0x4C566A'
      red:     '0xBF616A'
      green:   '0xA3BE8C'
      yellow:  '0xEBCB8B'
      blue:    '0x81A1C1'
      magenta: '0xB48EAD'
      cyan:    '0x8FBCBB'
      white:   '0xECEFF4'

  one_dark: &one_dark
    primary:
      background: '0x1e2127'
      foreground: '0xabb2bf'
    normal:
      black:   '0x1e2127'
      red:     '0xe06c75'
      green:   '0x98c379'
      yellow:  '0xd19a66'
      blue:    '0x61afef'
      magenta: '0xc678dd'
      cyan:    '0x56b6c2'
      white:   '0xabb2bf'
    bright:
      black:   '0x5c6370'
      red:     '0xe06c75'
      green:   '0x98c379'
      yellow:  '0xd19a66'
      blue:    '0x61afef'
      magenta: '0xc678dd'
      cyan:    '0x56b6c2'
      white:   '0xffffff'

  solarized_dark: &solarized_dark
    primary:
      background: '0x002b36'
      foreground: '0x839496'
    normal:
      black:   '0x073642'
      red:     '0xdc322f'
      green:   '0x859900'
      yellow:  '0xb58900'
      blue:    '0x268bd2'
      magenta: '0xd33682'
      cyan:    '0x2aa198'
      white:   '0xeee8d5'
    bright:
      black:   '0x002b36'
      red:     '0xcb4b16'
      green:   '0x586e75'
      yellow:  '0x657b83'
      blue:    '0x839496'
      magenta: '0x6c71c4'
      cyan:    '0x93a1a1'
      white:   '0xfdf6e3'

  solarized_light: &solarized_light
    primary:
      background: '0xfdf6e3'
      foreground: '0x586e75'
    normal:
      black:   '0x073642'
      red:     '0xdc322f'
      green:   '0x859900'
      yellow:  '0xb58900'
      blue:    '0x268bd2'
      magenta: '0xd33682'
      cyan:    '0x2aa198'
      white:   '0xeee8d5'
    bright:
      black:   '0x002b36'
      red:     '0xcb4b16'
      green:   '0x586e75'
      yellow:  '0x657b83'
      blue:    '0x839496'
      magenta: '0x6c71c4'
      cyan:    '0x93a1a1'
      white:   '0xfdf6e3'

  terminal_app: &terminal_app
    primary:
      background: '0x000000'
      foreground: '0xb6b6b6'
    normal:
      black:   '0x000000'
      red:     '0x990000'
      green:   '0x00a600'
      yellow:  '0x999900'
      blue:    '0x0000b2'
      magenta: '0xb200b2'
      cyan:    '0x00a6b2'
      white:   '0xbfbfbf'
    bright:
      black:   '0x666666'
      red:     '0xe50000'
      green:   '0x00d900'
      yellow:  '0xe5e500'
      blue:    '0x0000ff'
      magenta: '0xe500e5'
      cyan:    '0x00e5e5'
      white:   '0xe5e5e5'

  tomorrow_night: &tomorrow_night
    primary:
      background: '0x1d1f21'
      foreground: '0xc5c8c6'
    cursor:
      text: '0x1d1f21'
      cursor: '0xffffff'
    normal:
      black:   '0x1d1f21'
      red:     '0xcc6666'
      green:   '0xb5bd68'
      yellow:  '0xe6c547'
      blue:    '0x81a2be'
      magenta: '0xb294bb'
      cyan:    '0x70c0ba'
      white:   '0x373b41'
    bright:
      black:   '0x666666'
      red:     '0xff3334'
      green:   '0x9ec400'
      yellow:  '0xf0c674'
      blue:    '0x81a2be'
      magenta: '0xb77ee0'
      cyan:    '0x54ced6'
      white:   '0x282a2e'

  xterm: &xterm
    primary:
      background: '0x000000'
      foreground: '0xffffff'
    normal:
      black:   '0x000000'
      red:     '0xcd0000'
      green:   '0x00cd00'
      yellow:  '0xcdcd00'
      blue:    '0x0000ee'
      magenta: '0xcd00cd'
      cyan:    '0x00cdcd'
      white:   '0xe5e5e5'
    bright:
      black:   '0x7f7f7f'
      red:     '0xff0000'
      green:   '0x00ff00'
      yellow:  '0xffff00'
      blue:    '0x5c5cff'
      magenta: '0xff00ff'
      cyan:    '0x00ffff'
      white:   '0xffffff'

  thinkpadred: &thinkpadred
    primary:
      background: '#171717'
      foreground: '#eeeeee'
    normal:
      black:   '#000000'
      blue:    '#912d2d'
      magenta: '#a12529'
      red:     '#cc2929' # thinkpad red
      green:   '#ca131e'
      cyan:    '#c11720'
      yellow:  '#e20817'
      white:   '#777777'
    bright:
      black:   '#000000'
      blue:    '#c93c3c'
      magenta: '#cf2d33'
      red:     '#e82e2e' # thinkpad red
      green:   '#e61521'
      cyan:    '#e61924'
      yellow:  '#f50718'
      white:   '#777777'

  justblue: &justblue
    primary:
      background: '#171717'
      foreground: '#eeeeee'
    normal:
      black:   '#000000'
      blue:    '#2d6791'
      magenta: '#256da1'
      red:     '#2988cc'
      green:   '#127dc9'
      cyan:    '#177bc2'
      yellow:  '#0988e3'
      white:   '#777777'
    bright:
      black:   '#000000'
      blue:    '#3c8ec9'
      magenta: '#2d8ccf'
      red:     '#2e9be8'
      green:   '#158fe6'
      cyan:    '#1991e6'
      yellow:  '#0792f5'
      white:   '#777777'

#colors: *xterm
#colors: *justblue
#colors: *thinkpadred
colors: *nord
