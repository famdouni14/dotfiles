# Please see http://i3wm.org/docs/userguide.html for a complete reference!

# Autostart w/ i3 #######################################################################

# General
exec_always --no-startup-id fix_xcursor
exec --no-startup-id xmodmap ~/.Xmodmap

# display
exec --no-startup-id "picom"
exec --no-startup-id zsh ~/.config/polybar/launch_polybar.sh
exec --no-startup-id "nitrogen --restore"
exec --no-startup-id "unclutter --timeout 5 --jitter 5 --ignore-scrolling"

# audio
exec --no-startup-id zsh ~/Userdata/Scripts/Launchers/restart_pulseaudio.sh
exec --no-startup-id zsh -c "pactl unload-module module-role-cork"

# extra
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec --no-startup-id "rfkill unblock bluetooth"
exec --no-startup-id blueberry-tray
#exec --no-startup-id onboard
exec --no-startup-id nm-applet
#exec --no-startup-id zsh ~/Userdata/Scripts/Launchers/launch_insync.sh
exec --no-startup-id "nextcloud --background"


################################################################################################
# General Hotkeys

# Set mod key (Mod1=<Alt>, Mod4=<Super>)
set $mod Mod4

# set default desktop layout (default is tiling)
# workspace_layout tabbed <stacking|tabbed>

# Configure border style <normal|1pixel|pixel xx|none|pixel>
new_window pixel 1
new_float pixel 1
default_border pixel 1

# Hide borders
#hide_edge_borders none

# Change Borders
bindsym $mod+u border none
bindsym $mod+y border pixel 2
bindsym $mod+n border normal

# Font for window titles
font xft:Roboto Mono 11

# Use Mouse+$mod to drag floating windows
floating_modifier $mod

# Kill focused window
bindsym $mod+Shift+q kill

# Terminal
bindsym $mod+Return exec --no-startup-id alacritty

# System Monitor
bindsym Control+Shift+Escape exec --no-startup-id "alacritty -e sh -c 'htop'"

# Type Clipboad
bindsym $mod+Shift+v exec --no-startup-id sh ~/Userdata/Scripts/Misc/xclip_type.sh


################################################################################################
# Program Launchers

# rofi
bindsym $mod+Shift+d exec --no-startup-id "bwmenu"
bindsym $mod+d exec --no-startup-id "rofi -show drun -show-icons -icon-theme Adwaita -drun-display-format {name} -sort -sort-method fzf"
bindsym $mod+Tab exec --no-startup-id "rofi -show window -show-icons -icon-theme Adwaita -drun-display-format {name} -sort -sort-method fzf"

# dmenu
#bindsym $mod+d exec --no-startup-id dmenu_recency

################################################################################################
# Music

bindsym $mod+slash exec --no-startup-id "playerctl play-pause"
bindsym $mod+period exec --no-startup-id "playerctl next"
bindsym $mod+comma  exec --no-startup-id "playerctl previous"

exec --no-startup-id pulseaudio
#exec --no-startup-id pa-applet
bindsym $mod+Ctrl+m exec --no-startup-id pavucontrol

# Pulse Audio controls
# run amixer sset Master on
bindsym XF86AudioMute exec "amixer sset 'Master' toggle;"
bindsym XF86AudioLowerVolume exec "amixer sset 'Master' 5%-;"
bindsym XF86AudioRaiseVolume exec "amixer sset 'Master' 5%+;"


################################################################################################
# Screen

# Hide / Unhide statusbar
bindsym $mod+m exec --no-startup-id polybar-msg cmd toggle

bindsym XF86MonBrightnessUp exec "xbacklight -inc 10"
bindsym XF86MonBrightnessDown exec "xbacklight -dec 10"


################################################################################################
# Applications

# Screenshot [fullscreen, select area, select area with 3 second delay]
bindsym Print exec --no-startup-id "flameshot gui"
bindsym $mod+Print exec --no-startup-id "flameshot full"
#bindsym $mod+Shift+Print exec --no-startup-id deepin-screenshot -d 3 -s /home/sim/Pictures/Screenshots

# Firefox
bindsym $mod+F2 exec --no-startup-id firefox
bindsym $mod+Shift+F2 exec --no-startup-id "firefox google.com"
bindsym $mod+Shift+F4 exec --no-startup-id "firefox -new-window web.whatsapp.com"

# Nautilus
bindsym $mod+F3 exec --no-startup-id thunar
bindsym $mod+Ctrl+x --release exec --no-startup-id xkill

# Telegram
bindsym $mod+F4 exec telegram-desktop


################################################################################################
# Window Management

focus_follows_mouse yes

# move focus arrow keys
bindsym $mod+Left focus left
bindsym $mod+Right focus right
bindsym $mod+Up focus up
bindsym $mod+Down focus down

# move focused window arrow keys
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# workspace back and forth (with/without active container)
workspace_auto_back_and_forth yes
bindsym $mod+b workspace back_and_forth
bindsym $mod+Shift+b move container to workspace back_and_forth; workspace back_and_forth

# split orientation
bindsym $mod+h split h;exec notify-send 'tile horizontally'
bindsym $mod+v split v;exec notify-send 'tile vertically'
bindsym $mod+q split toggle

# toggle fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+i layout stacking
bindsym $mod+o layout tabbed
bindsym $mod+p layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# toggle sticky
bindsym $mod+Shift+s sticky toggle

#navigate workspaces next / previous
bindsym $mod+Ctrl+Right workspace next
bindsym $mod+Ctrl+Left workspace prev


################################################################################################
# Workspaces

# Workspace names
# to display names or symbols instead of plain workspace numbers you can use
# something like: set $ws1 1:mail
#                 set $ws2 2:
set $ws1 1
set $ws2 2
set $ws3 3
set $ws4 4
set $ws5 5
set $ws6 6
set $ws7 7
set $ws8 8
#set $ws9 9
#set $ws10 10
set $ws9 9:
set $ws10 10:#

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# Move focused container to workspace
bindsym $mod+Ctrl+1 move container to workspace $ws1
bindsym $mod+Ctrl+2 move container to workspace $ws2
bindsym $mod+Ctrl+3 move container to workspace $ws3
bindsym $mod+Ctrl+4 move container to workspace $ws4
bindsym $mod+Ctrl+5 move container to workspace $ws5
bindsym $mod+Ctrl+6 move container to workspace $ws6
bindsym $mod+Ctrl+7 move container to workspace $ws7
bindsym $mod+Ctrl+8 move container to workspace $ws8
bindsym $mod+Ctrl+9 move container to workspace $ws9
bindsym $mod+Ctrl+0 move container to workspace $ws10

# Move to workspace with focused container
bindsym $mod+Shift+1 move container to workspace $ws1; workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2; workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3; workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4; workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5; workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6; workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7; workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8; workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9; workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10; workspace $ws10


################################################################################################
# App / Window specific settings

# Open applications on specific workspaces
assign [class="(?i)virtualbox"] $ws10

# Open specific applications in floating mode
for_window [class="Lightdm-settings"] floating enable
for_window [class="Lxappearance"] floating enable sticky enable border normal
for_window [class="^.*"] none

for_window [title="Qalculate!"] floating enable border normal
for_window [title="Insync"] floating enable
for_window [title="Volume Control"] floating enable
for_window [title="xopp 2 pdf"] floating enable
for_window [title="Microsoft Teams Notification"] floating enable

# switch to workspace with urgent window automatically
for_window [urgent=latest] focus

# reload the configuration file
#bindsym $mod+Shift+o reload
bindsym $mod+Shift+o exec --no-startup-id "sh ~/.config/i3/reload_w_polybar.sh"

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+i reload


################################################################################################
# Shutdown / Resize Menu

# Set shut down, restart and locking features
bindsym $mod+Escape mode "$mode_system"
set $mode_system |  [s]hutdown , [r]eboot , [p]owersave , [h]ibernate , [l]ock , [e]xit
mode "$mode_system" {
    bindsym s exec --no-startup-id bash ~/Userdata/Scripts/Display/glitchlock/glitchlock.sh shutdown, mode "default"
    bindsym r exec --no-startup-id bash ~/Userdata/Scripts/Display/glitchlock/glitchlock.sh reboot, mode "default"
    bindsym p exec --no-startup-id bash ~/Userdata/Scripts/Display/glitchlock/glitchlock.sh suspend, mode "default"
    bindsym h exec --no-startup-id bash ~/Userdata/Scripts/Display/glitchlock/glitchlock.sh hibernate, mode "default"
    bindsym l exec --no-startup-id bash ~/Userdata/Scripts/Display/glitchlock/glitchlock.sh lock, mode "default"
    bindsym e exec --no-startup-id bash ~/Userdata/Scripts/Display/glitchlock/glitchlock.sh logout, mode "default"

    # exit system mode: "Enter" or "Escape"
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+Escape mode "default"
}

# Resize window (you can also use the mouse for that)
bindsym $mod+r mode "|  Resize "
mode "|  Resize " {
        # These bindings trigger as soon as you enter the resize mode
        # for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        bindsym Shift+Left resize shrink width 50 px or 50 ppt
        bindsym Shift+Down resize grow height 50 px or 50 ppt
        bindsym Shift+Up resize shrink height 50 px or 50 ppt
        bindsym Shift+Right resize grow width 50 px or 50 ppt

        # exit resize mode: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}


################################################################################################
# Themes / Colors / Design / Look and Feel

# Colors
# class                   border    backgr.     text        indic.   child_border
  client.focused          #5895e0   #5895e0     #ffffff     #5e81ac
  client.focused_inactive #81a1c1   #81a1c1     #ffffff     #81a1c1
  client.unfocused        #81a1c1   #81a1c1     #ffffff     #81a1c1
  client.urgent           #b48ead   #b48ead     #ffffff     #b48ead
  client.placeholder      #88c0d0   #88c0d0     #ffffff     #000000 

  client.background       #2B2C2B

################################################
# i3-gaps

# Set inner/outer gaps
gaps inner 25
gaps outer 5

# Additionally, you can issue commands with the following syntax. This is useful to bind keys to changing the gap size.
# gaps inner|outer current|all set|plus|minus <px>
# gaps inner all set 10
# gaps outer all plus 5

# Smart gaps (gaps used if only more than one container on the workspace)
smart_gaps on

# Smart borders (draw borders around container only if it is not the only container on this workspace) 
# on|no_gaps (on=always activate and no_gaps=only activate if the gap size to the edge of the screen is 0)
smart_borders on

# Press $mod+Shift+g to enter the gap mode. Choose o or i for modifying outer/inner gaps. Press one of + / - (in-/decrement for current workspace) or 0 (remove gaps for current workspace). If you also press Shift with these keys, the change will be global for all workspaces.
set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
bindsym $mod+Shift+g mode "$mode_gaps"

set $toggle_gaps Toggle gaps: (0) off / (1) on 
bindsym $mod+g mode "$toggle_gaps"
mode "$toggle_gaps" {
    bindsym 1 mode "default", gaps inner all set 25, gaps outer all set 5
    bindsym 0 mode "default", gaps inner all set 0, gaps outer all set 0
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_inner" {
        bindsym plus  gaps inner current plus 5
        bindsym minus gaps inner current minus 5
        bindsym 0     gaps inner current set 0

        bindsym Shift+plus  gaps inner all plus 5
        bindsym Shift+minus gaps inner all minus 5
        bindsym Shift+0     gaps inner all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_outer" {
        bindsym plus  gaps outer current plus 5
        bindsym minus gaps outer current minus 5
        bindsym 0     gaps outer current set 0

        bindsym Shift+plus  gaps outer all plus 5
        bindsym Shift+minus gaps outer all minus 5
        bindsym Shift+0     gaps outer all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

################################################
# i3-gaps-rounded

#border_radius 10
