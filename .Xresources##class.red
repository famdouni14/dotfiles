Xft.antialias: true
Xft.hinting:   true
Xft.rgba:      rgb
Xft.hintstyle: hintfull
Xft.dpi:	140
Xft.autohint:  true
Xft.lcdfilter: lcddefault

! Remove me
!XTerm*background:        #000000
!XTerm*foreground:        #fafafa
!XTerm*pointerColor:      #1ABB9B
!XTerm*faceName:          Fixed
!XTerm*faceSize:          11
!XTerm*reverseVideo:      on
!XTerm*selectToClipboard: true

*fading:                          8
*fadeColor:                       black
*pointerColorBackground:          #2B2C2B
*pointerColorForeground:          #16A085

! Thinkpad Red

! special
*.foreground:   #ffffff
*.background:   #171717
*.cursorColor:  #ffffff

! black
*.color0:       #000000
*.color8:       #090909

! red
*.color1:       #cc2929
*.color9:       #e60012

! green
*.color2:       #cc2929
*.color10:      #e60012

! yellow
*.color3:       #cc2929
*.color11:      #e60012

! blue
*.color4:       #cc2929
*.color12:      #e60012

! magenta
*.color5:       #cc2929
*.color13:      #e60012

! cyan
*.color6:       #cc2929
*.color14:      #e60012

! white
*.color7:       #777777
*.color15:      #707070

Xcursor.theme: xcursor-breeze-snow
Xcursor.size:                     0

! for 'fake' transparency (without Compton) use the following three lines
! URxvt*inheritPixmap:            true
! URxvt*transparent:              true
! URxvt*shading:                  125

URxvt*foreground:                 grey

URxvt*depth:					  32
URxvt*background:                 [90]#171717
URxvt*scrollBar:                  false
URxvt*mouseWheelScrollPage:       false
URxvt*cursorBlink:                false
URxvt*saveLines:                  5000

! Normal copy-paste keybindings without perls
URxvt.iso14755:                   false
URxvt.keysym.Shift-Control-V:     eval:paste_clipboard
URxvt.keysym.Shift-Control-C:     eval:selection_to_clipboard
!Xterm escape codes, word by word movement
URxvt.keysym.Control-Left:        \033[1;5D
URxvt.keysym.Shift-Control-Left:  \033[1;6D
URxvt.keysym.Control-Right:       \033[1;5C
URxvt.keysym.Shift-Control-Right: \033[1;6C
URxvt.keysym.Control-Up:          \033[1;5A
URxvt.keysym.Shift-Control-Up:    \033[1;6A
URxvt.keysym.Control-Down:        \033[1;5B
URxvt.keysym.Shift-Control-Down:  \033[1;6B

! Find Font with cmd: fc-list -v | grep "Roboto"
URxvt*font:                       xft:Hack Nerd Font Mono:antialias=true
URxvt*boldFont:                   xft:Hack Nerd Font Mono:style=bold:antialias=true
